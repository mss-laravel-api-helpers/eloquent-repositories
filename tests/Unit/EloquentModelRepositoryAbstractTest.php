<?php
/**
 * Unit test for EloquentModelAbstract
 */

namespace Tests\Unit;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mockery\MockInterface;
use Nwilging\EloquentRepositories\Models\EloquentModelAbstract;
use Nwilging\EloquentRepositories\Repositories\EloquentModelRepositoryAbstract;
use Nwilging\EloquentRepositories\Tests\TestCase;
use Psr\Log\LoggerInterface;

/**
 * Class EloquentModelRepositoryAbstractTest
 * @package Tests\Unit
 */
class EloquentModelRepositoryAbstractTest extends TestCase
{
    /**
     * @var EloquentModelRepositoryAbstract
     */
    protected $repository;

    /**
     * @var MockInterface
     */
    protected $mockModel;

    /**
     * @var MockInterface
     */
    protected $log;

    protected function setUp(): void
    {
        parent::setUp();

        $mockModel = \Mockery::mock(EloquentModelAbstract::class);
        $this->log = \Mockery::mock(LoggerInterface::class);
        $this->mockModel = $mockModel;
        $this->repository = new class($mockModel, $this->log) extends EloquentModelRepositoryAbstract {};
    }

    public function testFindById()
    {
        $id = 1;

        $this->mockModel->shouldReceive('with')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('find')->once()->with($id)->andReturnSelf();

        $this->repository->findById($id);
    }

    public function testFindByIdOrFailSuccess()
    {
        $id = 1;
        $this->mockModel->shouldReceive('with')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('findOrFail')->once()->with($id)->andReturnSelf();

        $this->repository->findByIdOrFail($id);
    }

    public function testFindByIdOrFailThrowsException()
    {
        $this->expectException(ModelNotFoundException::class);
        $id = 1;
        $this->mockModel->shouldReceive('with')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('findOrFail')->once()->with($id)->andThrow(ModelNotFoundException::class);

        $this->repository->findByIdOrFail($id);
    }

    public function testFindByIdWith()
    {
        $id = 1;
        $with = ['testWith'];

        $this->mockModel->shouldReceive('with')->once()->with($with)->andReturnSelf();
        $this->mockModel->shouldReceive('find')->once()->with($id)->andReturnSelf();

        $this->repository->findById($id, $with);
    }

    public function testFindAllBare()
    {
        $this->mockModel->shouldReceive('with')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('where')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('limit')->once()->with(null)->andReturnSelf();
        $this->mockModel->shouldReceive('get')->once()->andReturn(new Collection());

        $this->repository->findAll();
    }

    public function testFindAllWhereBasic()
    {
        $wheres = [
            ['col', '=', 'val'],
            ['col2', '<', 'val2']
        ];

        $this->mockModel->shouldReceive('with')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('where')->once()->with($wheres)->andReturnSelf();
        $this->mockModel->shouldReceive('limit')->once()->with(null)->andReturnSelf();
        $this->mockModel->shouldReceive('get')->once()->andReturn(new Collection());

        $this->repository->findAll($wheres);
    }

    public function testFindAllWhereComplex()
    {
        $simpleWheres = [
            ['col', '=', 'val']
        ];

        $complexWheres = [
            ['col2', 'in', ['val1', 'val2', 'val3']],
            ['col3', 'not in', ['val10', 'val11', 'val12']]
        ];

        $allWheres = array_merge($simpleWheres, $complexWheres);

        $this->mockModel->shouldReceive('with')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('where')->once()->with($simpleWheres)->andReturnSelf();
        $this->mockModel->shouldReceive('whereIn')->once()->with('col2', ['val1', 'val2', 'val3'])->andReturnSelf();
        $this->mockModel->shouldReceive('whereNotIn')->once()->with('col3', ['val10', 'val11', 'val12'])->andReturnSelf();

        $this->mockModel->shouldReceive('limit')->once()->with(null)->andReturnSelf();
        $this->mockModel->shouldReceive('get')->once()->andReturn(new Collection());

        $this->repository->findAll($allWheres);
    }

    public function testFindAllWith()
    {
        $this->mockModel->shouldReceive('with')->once()->with(['relationship'])->andReturnSelf();
        $this->mockModel->shouldReceive('where')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('limit')->once()->with(null)->andReturnSelf();
        $this->mockModel->shouldReceive('get')->once()->andReturn(new Collection());

        $this->repository->findAll([], ['relationship']);
    }

    public function testFindAllLimit()
    {
        $this->mockModel->shouldReceive('with')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('where')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('limit')->once()->with(10)->andReturnSelf();
        $this->mockModel->shouldReceive('get')->once()->andReturn(new Collection());

        $this->repository->findAll([], [], 10);
    }

    public function testFindAllOrderBy()
    {
        $this->mockModel->shouldReceive('with')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('where')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('limit')->once()->with(null)->andReturnSelf();
        $this->mockModel->shouldReceive('orderBy')->once()->with('column', 'ASC')->andReturnSelf();
        $this->mockModel->shouldReceive('get')->once()->andReturn(new Collection());

        $this->repository->findAll([], [], null, ['column' => 'ASC']);
    }

    public function testFindAllMultipleOrderBy()
    {
        $this->mockModel->shouldReceive('with')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('where')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('limit')->once()->with(null)->andReturnSelf();
        $this->mockModel->shouldReceive('orderBy')->once()->with('column', 'ASC')->andReturnSelf();
        $this->mockModel->shouldReceive('orderBy')->once()->with('anotherCol', 'DESC')->andReturnSelf();
        $this->mockModel->shouldReceive('get')->once()->andReturn(new Collection());

        $this->repository->findAll([], [], null, [
            'column' => 'ASC',
            'anotherCol' => 'DESC'
        ]);
    }

    public function testFindAllInvalidOrderByDirection()
    {
        $this->mockModel->shouldReceive('with')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('where')->once()->with([])->andReturnSelf();
        $this->mockModel->shouldReceive('limit')->once()->with(null)->andReturnSelf();
        $this->mockModel->shouldReceive('get')->once()->andReturn(new Collection());

        $this->mockModel->shouldNotReceive('orderBy');

        $this->repository->findAll([], [], null, ['column' => 'invalidDirection']);
    }
}
