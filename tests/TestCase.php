<?php
/**
 * Base test case
 */

namespace Nwilging\EloquentRepositories\Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\DB;

/**
 * Class TestCase
 * @package Nwilging\EloquentRepositories\Tests
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * @var bool
     */
    protected static $migrated = false;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app['config']->set('database.default','sqlite');
        $this->app['config']->set('database.connections.sqlite.database', ':memory:');

        $this->setupDatabase();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    protected function setupDatabase()
    {
        DB::unprepared(file_get_contents(__DIR__ . '/testing.sql'));
    }
}