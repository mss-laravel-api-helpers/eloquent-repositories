<?php
/**
 * Model for testing relationships
 */

namespace Nwilging\EloquentRepositories\Tests\TestModels;

use Nwilging\EloquentRepositories\Models\EloquentModelAbstract;

class TestModelB extends EloquentModelAbstract
{
    public $primaryKey = 'id';
    public $table = 'test_model_b';

    public $timestamps = false;

    protected $fillable = ['name', 'model_a_id'];

    public function testModelA()
    {
        return $this->belongsTo(TestModelA::class, 'model_a_id');
    }
}