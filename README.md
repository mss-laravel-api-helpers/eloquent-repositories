# Installation

To use this package in your project, you must first add it to your repositories configuration:
```
repositories: [
    {
        "type": "vcs",
        "url": "https://nwilging@bitbucket.org/mss-laravel-api-helpers/eloquent-repositories.git"
    }
]
```

Then require it in your project:
```
composer require nwilging/eloquent-repositories
```

In Laravel < 5.4 (I believe), you must add the service provider to `config/app.php`:
```
providers => [
    ...
    Nwilging\EloquentRepositories\EloquentRepositoryServiceProvider::class,
    ...
]
```

# Repository Pattern

Familiarize yourself with the repository pattern in Laravel. If you are going to use Eloquent Repositories, you should also follow the guidelines set forth by this readme.

1. Every repository should be accompanied by a contract
2. Every repository must be instantiated with a model

These guidelines may or may not go against basic eloquent repository flow. However, they are very easy to implement and should not cause any issues. The updated repository pattern
used here is very slim and easy to use.

# Creating Models

All of your models should extend `EloquentModelAbstract`. This abstract model extends the base eloquent model.

```
class MyModel extends Nwilging\EloquentRepositories\Models\EloquentModelAbstract
{
    ...
}
```

# Creating Repositories

There are two components to putting together a repository: the contract, and the repository.

## The Contract

Place all contracts in `App\Contracts\Repositories` namespace to comply with repository pattern. Every contract should extend `EloquentModelRepositoryContract`:

```
interface MyRepositoryContract extends Nwilging\EloquentRepositories\Contracts\Repositories\EloquentModelRepositoryContract
{
    ...
}
}
```

## The Repository Implementation

Repository implementations should be created in `App\Repositories` namespace to comply with repository pattern. Every repository should implement your extension of
`EloquentModelRepositoryContract` and should extend `EloquentModelRepositoryAbstract` to provide the basic repository methods for the contract:

```
class MyRepository extends Nwilging\EloquentRepositories\Repositories\EloquentModelRepositoryAbstract implements App\Contracts\Repositories\MyRepositoryContract
{
    ...
}
}
```

## Type-hinting Repositories

By default, you do not need a constructor in your new repository. If your repository should only use basic CRUD methods, then there is no reason to have any body to the class
at all. Best practice dictates that you should include a constructor simply so that you understand which model the repository uses. More on instantiating repositories later.

```
public function __construct(MyModelClass $model)
{
    parent::__construct($model);
}
}
```

# Binding/Registering in Provider

It is best (in my opinion) to register repositories within a provider's `register` or `boot` method:

```
$this->app->bind(MyRepositoryContract::class, function () {
    return new MyRepository(new MyModelClass());
});
```

This will allow the repository to be injected using Laravel's dependency injection container. There are plenty of resources out there that can give you more information on the
container and how to use it. We will not go over that here.

# Repository Methods

`$repository->create(array ... $data): Model;` Creates record for model. Accepts multiple arrays which will be merged into one before create.

`$repository->update($model, array ... $data): Model;` Updates record for provided model. Accepts multiple arrays which will be merged into one before update.

`$repository->delete($model): void;` Deletes a record for provided model.

`$repository->findAll(array $where = [], array $with = [], int $limit = 0): Collection` Finds all records with provided parameters/constraints. Returns eloquent collection.

`$repository->findById($id): Model;` Finds single model record by a given ID.