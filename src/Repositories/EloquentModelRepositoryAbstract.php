<?php
/**
 * Abstract class for Eloquent model repositories
 */

namespace Nwilging\EloquentRepositories\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Nwilging\EloquentRepositories\Contracts\Repositories\EloquentModelRepositoryContract;
use Nwilging\EloquentRepositories\Models\EloquentModelAbstract;
use Psr\Log\LoggerInterface as LogContract;

/**
 * Class EloquentModelRepositoryAbstract
 * @package Nwilging\EloquentRepositories\Repositories
 */
abstract class EloquentModelRepositoryAbstract implements EloquentModelRepositoryContract
{
    /**
     * @const allowed orderBy clause strings
     */
    protected const ALLOWED_ORDERBY = [
        'ASC',
        'DESC'
    ];

    /**
     * @var EloquentModelAbstract
     */
    protected $model;

    /**
     * @var LogContract
     */
    protected $log;

    /**
     * EloquentModelRepositoryAbstract constructor.
     * @param EloquentModelAbstract $model
     * @param LogContract $log
     */
    public function __construct(EloquentModelAbstract $model, LogContract $log)
    {
        $this->model = $model;
        $this->log = $log;
    }

    /**
     * @param $id
     * @param array $with
     * @return EloquentModelAbstract
     */
    public function findById($id, array $with = []): EloquentModelAbstract
    {
        return $this->model->with($with)->find($id);
    }

    /**
     * @param $id
     * @param array $with
     * @return EloquentModelAbstract
     */
    public function findByIdOrFail($id, array $with = []): EloquentModelAbstract
    {
        return $this->model->with($with)->findOrFail($id);
    }

    /**
     * @param array $where
     * @param array $with
     * @param int|null $limit
     * @param array $orderBy
     * @return Collection
     */
    public function findAll(array $where = [], array $with = [], int $limit = null, array $orderBy = []): Collection
    {
        $results = $this->model->with($with);

        $whereIn = array_filter($where, function (array $whereClause) {
            return $whereClause[1] == 'in';
        });
        foreach ($whereIn as $in) {
            $results->whereIn($in[0], $in[2]);
        }

        $whereNotIn = array_filter($where, function (array $whereClause) {
            return $whereClause[1] == 'not in';
        });
        foreach ($whereNotIn as $notIn) {
            $results->whereNotIn($notIn[0], $notIn[2]);
        }

        $wheres = array_filter($where, function (array $whereClause) {
            return ($whereClause[1] != 'in' && $whereClause[1] != 'not in');
        });

        $results->where($wheres);

        if (!empty($orderBy)) {
            foreach ($orderBy as $column => $type) {
                if (!in_array($type, self::ALLOWED_ORDERBY)) {
                    // @TODO: throw exception?
                    continue;
                }
                $results->orderBy($column, $type);
            }
        }

        return $results->limit($limit)->get();
    }

    /**
     * @param array ...$data
     * @return EloquentModelAbstract
     */
    public function create(array ...$data): EloquentModelAbstract
    {
        $model = $this->model->create(array_merge(...$data));
        $this->log->info('Created model', [
            'model' => $model
        ]);
        return $model;
    }

    /**
     * @param EloquentModelAbstract $model
     * @param array ...$data
     * @return EloquentModelAbstract
     */
    public function update(EloquentModelAbstract $model, array ...$data): EloquentModelAbstract
    {
        $model->update(array_merge(...$data));
        $model->save();
        $this->log->info('Updated model', [
            'model' => $model
        ]);
        return $model;
    }

    /**
     * @param EloquentModelAbstract $model
     * @return bool
     * @throws \Exception
     */
    public function delete(EloquentModelAbstract $model): bool
    {
        $this->log->info('Deleted model', [
            'model' => $model
        ]);
        return $model->delete();
    }
}