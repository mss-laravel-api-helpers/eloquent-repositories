<?php
/**
 * Contract for the EloquentModelRepository
 */

namespace Nwilging\EloquentRepositories\Contracts\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Nwilging\EloquentRepositories\Models\EloquentModelAbstract;

/**
 * Interface EloquentModelRepositoryContract
 * @package Nwilging\EloquentRepositories\Contracts\Repositories
 */
interface EloquentModelRepositoryContract
{
    /**
     * Attempts to find a model by it's ID
     *
     * @param $id
     * @param $with
     * @return EloquentModelAbstract
     */
    public function findById($id, array $with = []): EloquentModelAbstract;

    /**
     * Attempts to find a model by its ID or throws ModelNotFoundException
     *
     * @param $id
     * @param array $with
     * @return EloquentModelAbstract
     */
    public function findByIdOrFail($id, array $with = []): EloquentModelAbstract;

    /**
     * Finds all records
     *
     * @param array $where
     * @param array $with
     * @param int|null $limit
     * @param array $orderBy
     * @return Collection
     */
    public function findAll(array $where = [], array $with = [], int $limit = null, array $orderBy = []): Collection;

    /**
     * Creates a record for the model
     *
     * @param \array[] ...$data
     * @return EloquentModelAbstract
     */
    public function create(array ...$data): EloquentModelAbstract;

    /**
     * Updates a model record with given data
     *
     * @param EloquentModelAbstract $model
     * @param \array[] ...$data
     * @return EloquentModelAbstract
     */
    public function update(EloquentModelAbstract $model, array ...$data): EloquentModelAbstract;

    /**
     * Deletes a model record
     *
     * @param EloquentModelAbstract $model
     * @return bool
     */
    public function delete(EloquentModelAbstract $model): bool;
}