<?php
/**
 * Abstract class for Eloquent models
 */

namespace Nwilging\EloquentRepositories\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EloquentModelAbstract
 * @package Nwilging\EloquentRepositories\Models
 */
abstract class EloquentModelAbstract extends Model {}